// JavaScript Document
jQuery.fn.wuDropdown = function(options) {
	var settings = $.extend({
		// 子菜单宽度
		subwidth : $(this).width() - 1
	}, options);
	function openSubMenu() {
		$(this).find('ul').width(settings.subwidth);
		$(this).find('ul').css('visibility', 'visible');
	}
	;
	function closeSubMenu() {
		$(this).find('ul').css('visibility', 'hidden');
	}
	;
	$(this).bind('mouseover', openSubMenu);
	$(this).bind('mouseout', closeSubMenu);
};