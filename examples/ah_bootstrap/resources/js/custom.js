function goTop(){
    $(window).scroll(function(e) {
        //若滚动条离顶部大于100元素
        if($(window).scrollTop()>100)
            $("#gotop").fadeIn(1000);//以1秒的间隔渐显id=gotop的元素
        else
            $("#gotop").fadeOut(1000);//以1秒的间隔渐隐id=gotop的元素
    });
};
$(function(){
    //点击回到顶部的元素
    $("#gotop").click(function(e) {
            //以1秒的间隔返回顶部
            $('body,html').animate({scrollTop:0},1000);
    }).addClass("gotop_out").mouseover(function(e) {
    	$(this).removeClass("gotop_out").addClass("gotop_on");
        //$(this).css("background","url(../images/backtop2013.png) no-repeat 0px 0px");
    }).mouseout(function(e) {
    	$(this).removeClass("gotop_on").addClass("gotop_out");
        //$(this).css("background","url(../images/backtop2013.png) no-repeat -70px 0px");
    });
    goTop();//实现回到顶部元素的渐显与渐隐
    
    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(20).fadeIn(200);
      }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
      }); 
});