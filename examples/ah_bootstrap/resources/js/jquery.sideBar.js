/**
 * 侧边拦
 */
;(function ($, window, document,undefined) {
	"use strict";
	var _defaults={
			/**
			 * 点击其他时是否切换
			 * @property isToggle
			 * @type Boolean
			 * @default true
			 */
			isToggle: true,
			/**
			 * 切换时，显示的动画动作
			 * @property animate
			 * @type String("fast","normal","slow","close")
			 * @default "close"
			 */
			animate: "close",
			/**
			 * 菜单数据
			 * @property data
			 * @type JSON
			 * @default null
			 */
			data:null,
			/**
			 * 远程数据
			 * @type String
			 * @default null
			 */
			ajaxURL:null
	};
	//私有方法
	function _init(self){
		if(typeof (self.$element.attr("data-role"))=="undefined"||"sidenav"!=self.$element.attr("data-role")){
			self.$element.attr("data-role","sidenav");
		}
		var data=self.options.data;
		if(data){
			console.log(data);
			console.log("生成页面");
		}
		console.log(self.options);
	}
	//定义SideBar的构造函数
	var SideBar = function(element,opt) {
		this.$element = $(element);
		this.defaults=_defaults;
		this.options = $.extend({}, this.defaults, opt);
		_init(this);
	};
	//共有方法
	SideBar.prototype = {
		constructor:SideBar,
		debug:function(name){
			console.log(name);
			return name;
		},
		/**
		 * 切换方法
		 * @method toggle
		 */
		toggle:function(el,ev){
			var $this = $(el);
			if($this.parent().hasClass("has_sub")) {
		        ev.preventDefault();
			}   
			if(!$this.hasClass("subdrop")) {
				// hide any open menus and remove all other classes
				$("[data-role=sidenav] li ul").slideUp(350);
				$("[data-role=sidenav] li a").removeClass("subdrop");
				
				// open our new menu and add the open class
				$this.next("ul").slideDown(350);
				$this.addClass("subdrop");
			}else if($this.hasClass("subdrop")) {
				$this.removeClass("subdrop");
				$this.next("ul").slideUp(350);
			} 
		},

	};
  	var old = $.fn.sidebar;
  	
  	$.fn.sidebar = function (option,target,event) {
  		
  		return this.each(function () {
			var $this   = $(this);
			var data    = $this.data('ah.sidebar');
			var options = $.extend({}, SideBar.DEFAULTS, $this.data(), typeof option == 'object' && option);
			if (!data) $this.data('ah.sidebar', (data = new SideBar(this, options)));
			if (typeof option == 'string') data[option](target,event);
  		});
  	};
  	$.fn.sidebar.Constructor = SideBar;


  // COLLAPSE NO CONFLICT
  // ====================

  	$.fn.sidebar.noConflict = function () {
  		$.fn.sidebar = old;
  		return this;
	};
	//Click
	$(document).on('click.ah.sidebar.data-api', '[data-role=sidenav] > li > a', function (e) {
	    var $this = $(this);
	    var data = $this.data('ah.sidebar');
	    $this.sidebar('toggle',this,e);
	});
	
})(jQuery, window, document);