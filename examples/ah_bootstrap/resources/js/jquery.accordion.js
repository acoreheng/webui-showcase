/**
 * 多级手风琴菜单
 */
;(function ($, window, document,undefined) {
	"use strict";
	var _defaults={
			accordion: 'true',
			speed: 300,
			closedSign: '[+]',
			openedSign: '[-]',
			/**
			 * 菜单数据
			 * @property data
			 * @type JSON
			 * @default null
			 */
			data:null,
			/**
			 * 远程数据
			 * @type String
			 * @default null
			 */
			ajaxURL:null
	};
	//私有方法
	function _init(self){
		var $this=self.$element,opts=self.options;
		//click
		$this.find("li > a").on("click",function(ev){
			var $that = $(this);
			if($that.parent().find("ul").size() == 0) {
				 ev.preventDefault();
			}
			if(opts.accordion){
				//Do nothing when the list is open
				if(!$that.parent().find("ul").is(':visible')){
					var parents = $that.parent().parents("ul");
					var visible = $this.find("ul:visible");
					var close=true;//默认关闭
					visible.each(function(visibleIndex){
						close = true;//重置关闭
						parents.each(function(parentIndex){
							if(parents[parentIndex] == visible[visibleIndex]){
								close = false;
								return false;
							}
						});
						if(close){
							if($that.parent().find("ul") != visible[visibleIndex]){
								$(visible[visibleIndex]).slideUp(opts.speed, function(){
									$(this).parent("li").find("span:first").html(opts.closedSign);
								});
								
							}
						}
					});
				}
			}
			//下级菜单
			var $ul=$that.parent().find("ul:first");
			if($ul.is(":visible")){
				$ul.slideUp(opts.speed, function(){
					$that.parent("li").find("span:first").delay(opts.speed).html(opts.closedSign);
				});
			}else{
				$ul.slideDown(opts.speed, function(){
					$that.parent("li").find("span:first").delay(opts.speed).html(opts.openedSign);
				});
			}
		});
		console.log(self.options);
	}
	//生成页面
	function _html(self){
		var $this=self.$element,opts=self.options;
		if(typeof ($this.attr("data-role"))=="undefined"||"sidenav"!=$this.attr("data-role")){
			$this.attr("data-role","sidenav");
		}
		var data=self.options.data;
		if(data){
			var html=[];
			$.each(data,function(i,v){
				console.log(html.join());
				html.push('<li><a>');
				if(v.icon){
					html.push(v.icon);
				}
				html.push(v.text);
				html.push('</a>');
				html.push(_addNode(v.children));
				html.push('</li>');
			});
			$this.append(html.join(''));
		}
		$this.find("li").each(function() {
 			if($(this).find("ul").size() != 0){
 				//add the multilevel sign next to the link
 				$(this).find("a:first").append("<span class='right'>"+ opts.closedSign +"</span>");
 				
 				//avoid jumping to the top of the page when the href is an #
 				if($(this).find("a:first").attr('href') == "#"){
 		  			$(this).find("a:first").click(function(){return false;});
 		  		}
 			}
 		});
	}
	function _addNode(data){
		var html=[];
		if(data){
			html.push('<ul class="nav">');
			$.each(data,function(i,v){
				html.push('<li><a>');
				html.push(v.text);
				html.push('</a>');
				html.push(_addNode(v.children));
				html.push('</li>');
			});
			html.push('</ul>');
		}
		return html.join('');
	}
	//定义SideBar的构造函数
	var AccordionMenu = function(element,opt) {
		this.$element = $(element);
		this.defaults=_defaults;
		this.options = $.extend({}, this.defaults, opt);
		_html(this);
		_init(this);
		
	};
	//共有方法
	AccordionMenu.prototype = {
		constructor:AccordionMenu,
		debug:function(name){
			console.log(name);
			return name;
		}
	};
  	var old = $.fn.accordion;
  	
  	$.fn.accordion = function (option,target,event) {
  		
  		return this.each(function () {
			var $this   = $(this);
			var data    = $this.data('ah.accordion');
			var options = $.extend({}, _defaults, $this.data(), typeof option == 'object' && option);
			if (!data) $this.data('ah.accordion', (data = new AccordionMenu(this, options)));
			if (typeof option == 'string') data[option](target,event);
  		});
  	};
  	$.fn.accordion.Constructor = AccordionMenu;


  // COLLAPSE NO CONFLICT
  // ====================

  	$.fn.accordion.noConflict = function () {
  		$.fn.accordion = old;
  		return this;
	};
})(jQuery, window, document);