function is_email(s) {
	if (s.length > 100) {
  	return false;
  }
  var regu = "^(([0-9a-zA-Z]+)|([0-9a-zA-Z]+[_.0-9a-zA-Z-]*[0-9a-zA-Z]+))@([a-zA-Z0-9-]+[.])+([a-zA-Z]{2}|net|NET|com|COM|gov|GOV|mil|MIL|org|ORG|edu|EDU|int|INT)$";
  var re = new RegExp(regu);
  if (s.search(re) != -1) {
      return true;
  } else {
      return false;
  }
}
function Round(a_Num , a_Bit){
	return( Math.round(a_Num * Math.pow (10 , a_Bit)) / Math.pow(10 , a_Bit)) ;
}
// 必须为数字的判断
function is_number(s){
  var regu = "^([0-9]+[-0-9]*)$";
  var re = new RegExp(regu);
  if (s.search(re) != -1) {
      return true;
  } else {
      return false;
  }
}
// 是否为Double型
function is_double(s) {
	var sn = Math.abs(Number(s));
	if(sn >=0 ) return true;
	return false;
}
function is_select(s) {
	var obj = document.getElementById(s);
  if(obj!=null && (obj.type=="select-one" || obj.type=="select-multiple")) {
    for(var i = 0; i< obj.options.length; i++) {
     if(obj.options[i].selected && obj.options[i].value!="") {
     	return true;
     }
   }// end for
  }
	return false;
}
// 短时间格式2008-09-01
function is_date_time(str)
{
	var r = str.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
	if(r==null)return false;
	var d= new Date(r[1], r[3]-1, r[4]);
	return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]);
}
// 长时间格式2008-09-01 19:00:00
function is_long_date_time(str)
{
	var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2})(:(\d{1,2}))?$/;
	var r = str.match(reg);
	if(r==null)return false;

	var d= new Date(r[1], r[3]-1,r[4],r[5],r[6],00);
	return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]&&d.getHours()==r[5]&&d.getMinutes()==r[6]);
}
function isIE(){ // ie?
	if (window.navigator.userAgent.indexOf("MSIE")>=1) return true; 
	return false; 
}
function messagebox() {
	var url = "../apps/messagebox.php";
	window.open(url,'box','resizable=no,scrollbars=yes,width=500,height=300,left=100,top=50');
	return false;
}

/*
 * function show_printer(vurl) {
 * window.open(vurl,'printer_box','resizable=yes,scrollbars=yes,width=800,height=600,left=100,top=50');
 * return false; }
 */

function select_fill(obj,name,usekey)
{
	var name_obj = document.getElementById(name);
	if(name_obj != null)
	{
		if(usekey == undefined || usekey==false)
			name_obj.value = obj.options[obj.selectedIndex].text;
		else
			name_obj.value = obj.options[obj.selectedIndex].value;
	}
}
/*******************************************************************************
 * 基于JQuery的函数
 ******************************************************************************/
// 全选
function checkall(state)
{
	$("input[type='checkbox']").each(function() {
		this.checked = state;
	});
}

// 检修是否选中一个或多个,返回值为ids
function check_multi(idname,msg) {
	var ids = "";
	var comma = "";
	$("input[id="+idname+"]").each(function(){
		if(this.checked)
		{
			if(this.value!="")
			{
				ids +=comma + this.value;
				comma = ",";
			}

		}
	});
	if(ids == "" && msg != undefined) alert(msg);
	return ids;
}
// 检修是否选中一个，不允许选中多个,返回值为ids
function check_one(idname,msg) {
	var ids = "";
	var ci = 0;
	$("input[id="+idname+"]").each(function(){
		if(this.checked) { ids = this.value; ci++;}
	});
	if(ci != 1)
	{
		alert(msg);
		return "";
	} else {
		return ids;
	}
}
function reset_checkbox(idname)
{
	$("input[id="+idname+"]").each(function(){
		this.checked = false;
	});
}
/**
 * 保存对象值 eg:set_object({storehouse_id:"1",storehouse_name:"name"});
 */
function set_object(obj)
{
	$.each(obj,function(i,n){
		var fo = $("#"+i);
		if(fo!=undefined)
		{
			var type = fo.attr("type");// radio字段时 id=name_value name=name
										// value=value
			if(type == "radio")
			{
				fo.attr("checked",true);
			} else {
				fo.val(n);
			}
		}
	});
}

function parseQuery ( query ) {
   var Params = {};
   if ( ! query ) {return Params;}// return empty object
   var Pairs = query.split(/[;&\?]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) {continue;}
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      Params[key] = val;
   }
   return Params;
}

// 显示帮助
function show_helper(id,action)
{
	var top = event.clientY + document.body.scrollTop + 5;
	var left = event.clientX + document.body.scrollLeft + 5;
	var width = $(window).width();
	if(left + 300 > width) left = event.clientX - 300;
	var mouseoffset = {top:top , left:left};
	if($("#help_"+id).html() == "")
	{
		$("#"+id).html("<img src=\"../images/busy.gif\"><font color=#000000>正在加载内容,请稍后... ...</font>");
		$("#"+id).css(mouseoffset);
		try{ $("#"+id).bgIframe(); } catch(e) {}
		$("#"+id).show();
		var params = {id:id,action:action,cache:Math.random()};
		$.get("../system/help.php",params,function(data){
			$("#help_"+id).html(data);
			$("#"+id).html(data);
			try{ $("#"+id).bgIframe(); } catch(e) {}
			$("#"+id).show();
		});
	} else {
		$("#"+id).html($("#help_"+id).html());
		$("#"+id).css(mouseoffset);
		try{ $("#"+id).bgIframe(); } catch(e) {}
		$("#"+id).show();
	}
}
function hide_helper(id)
{
	$("#"+id).hide();
}

/*******************************************************************************
 * 弹窗口函数
 ******************************************************************************/
function hide_popup(idname)
{
	document.getElementById("_popup_div_"+idname).style.visibility = "hidden";
}

function div_popup(title,content,footer,idname)
{
	var urlroot = "../images/";
	var temp = '';
	temp += '<div style="float: left;height: 150px;width: 8px;background-image: url('+urlroot+'bg_1.gif);"></div>';
    temp += '<div style="font-family: Tahoma;text-align:left;float: left;height: 150px;width: 324px;background-image: url('+urlroot+'bg_2.gif);background-repeat: repeat-x;">';
    temp += '<div><h1 style="	float:left;font-size: 14px;color: #FFFFFF;margin: 0px;padding: 10px 0 13px 2px;line-height:17px;">'+title+'</h1><a href="#" onclick="hide_popup(\''+idname+'\');return false;" style="background-image: url('+urlroot+'close.gif);float: right;height: 19px;width: 42px;" onmouseover="javascript:this.style.backgroundPosition=\'bottom\'" onmouseleave="javascript:this.style.backgroundPosition=\'top\'"></a></div>';
    temp += '<div style="height:83px;padding: 0 0 0 2px;clear:both;">';
    temp += '<p style="font-family:Tahoma;font-size:12px;line-height:24px;width: 320px;margin:0px;padding: 0 0 0 12px;display:block;float:left;margin-top:2px;word-wrap:break-word;">'+content+'</p></div>';
    temp += '<div style="clear: both;text-align:right;">';
    temp += footer;
    temp += '</div></div><div style="float: left;height: 150px;width: 8px;background-image: url('+urlroot+'bg_1.gif);background-position: right;"></div>';

    return temp;
}

function show_popup(doc,title , content ,footer, idname,timeout,height)
{
	title = title.replace(/ /g,"&nbsp;");
	if(height == undefined) var height = 200;
	var html="<div id="+idname+" title="+(title)+"><p>"+content+"</p></div>";
	var buttons= {}
	if(typeof(footer)=="object") buttons = footer;
	$("#"+idname).dialog("destory");
	$("#span_event_response").append(html);
	var dialog = $("#"+idname).dialog({
		zIndex:-1,
		resizable: true,
		modal: false,
		width:350,
		height:height,
		position:["right","bottom"],
		show : "slide",
		buttons: footer
	});	

	if(timeout == undefined) {
		var timeout = 30000;
		if($("#__poup_timeout_").length) timeout = $("#__poup_timeout_").val()*1000;
	}
	setTimeout('$("#'+idname+'").dialog("destroy").remove();',30000);	
}

function sticky_popup(title,content,timeout)
{
	if(timeout == undefined) {
		var timeout = 30000;
		if($("#__poup_timeout_").length) timeout = $("#__poup_timeout_").val()*1000;
	}
	var options = {autoclose:timeout};
	var sticky_content = "";
	if(title != "") sticky_content += "<b>" + title + "</b><br>";
	sticky_content += content;
	$.sticky(sticky_content,options);
}
/*
 * var global_position_top = 0; var global_position_right = 0; function
 * show_popup(doc,title , content ,footer, idname) { var div_idname =
 * "_popup_div_" + idname; var pop_div = doc.getElementById(div_idname);
 * 
 * var pop_divname = doc.getElementById("_popup_divname_");
 * 
 * if(pop_divname == null || pop_divname == undefined) { global_position_top =
 * 0; global_position_right = 0; } var position = 190 + global_position_top; var
 * posright = 500 - global_position_right;
 * 
 * 
 * if(pop_div != null) { pop_div.innerHTML =
 * div_popup(title,content,footer,idname); pop_div.style.visibility = "visible";
 * setTimeout("hide_popup('"+idname+"');",30000); } else { popupDivHtml = '<div
 * name="_popup_divname_" id="'+div_idname+'"
 * onmousedown="MyMove.Move(\''+div_idname+'\',event,1);"
 * style="z-index:10000;filter:alpha(opacity=90);position: absolute; right:0;
 * bottom:0;color:#000;font-size: 12px;cursor:move;height: 150px;width:
 * 340px;">'; popupDivHtml += div_popup(title,content,footer,idname);
 * popupDivHtml += '</div><span id="_popup_divname_"></span>';
 * doc.body.insertAdjacentHTML("beforeEnd",popupDivHtml); global_position_top +=
 * 20;global_position_right += 20;
 * setTimeout("hide_popup('"+idname+"');",30000); } try
 * {$("#"+div_idname).bgiframe();}catch(e){} }
 */
var MyMove = new Tong_MoveDiv();

function Tong_MoveDiv()
{
 	  this.Move=function(Id,Evt,T)
 	  {
 	  	if(Id == "")
		{
			return;
		}
 	  	var o = document.getElementById(Id);
 	  	if(!o)
		{
			return;
		}
 	    evt = Evt ? Evt : window.event;
 	    o.style.position = "absolute";
 	    o.style.zIndex = 200;
 	    var obj = evt.srcElement ? evt.srcElement : evt.target;
 	    var w = o.offsetWidth;
 	    var h = o.offsetHeight;
 	    var l = o.offsetLeft;
 	    var t = o.offsetTop;
 	    var div = document.createElement("DIV");
 	    document.body.appendChild(div);
 	    div.style.cssText = "filter:alpha(Opacity=10,style=0);opacity:0.2;width:"+w+"px;height:"+h+"px;top:"+t+"px;left:"+l+"px;position:absolute;background:#000";
 	    div.setAttribute("id", Id +"temp");
 	    this.Move_OnlyMove(Id,evt,T);
 	}

 	this.Move_OnlyMove = function(Id,Evt,T)
 	{
 		  var o = document.getElementById(Id+"temp");
 		  if(!o)
		  {
			return;
		  }
 		  evt = Evt?Evt:window.event;
 		  var relLeft = evt.clientX - o.offsetLeft;
 		  var relTop = evt.clientY - o.offsetTop;
 		  if(!window.captureEvents)
 		  {
 		  	 o.setCapture();
 		  }
 		  else
 		  {
 		  	 window.captureEvents(Event.MOUSEMOVE|Event.MOUSEUP);
 		  }

 		  document.onmousemove = function(e)
 		  {
 		  	   if(!o)
			   {
			        return;
		       }
 		  	   e = e ? e : window.event;

 		  	   if(e.clientX - relLeft <= 0)
			   {
			      o.style.left = 0 +"px";
		       }
 		  	   else if(e.clientX - relLeft >= document.body.clientWidth - o.offsetWidth - 2)
			   {
			      o.style.left = (document.body.clientWidth - o.offsetWidth - 2) +"px";
			   }
 		  	   else
			   {
			   	   o.style.left = e.clientX - relLeft +"px";
			   }
 		  	  if(e.clientY - relTop <= 1)
			  {
			  	 o.style.top = 1 +"px";
			  }
 		  	  else if(e.clientY - relTop >= document.body.clientHeight - o.offsetHeight - 30)
			  {
			  	o.style.top = (document.body.clientHeight - o.offsetHeight - 30) +"px";
			  }
 		  	  else
			  {
			  	o.style.top = e.clientY - relTop +"px";
			  }

 		   }

 		   document.onmouseup = function()
 		   {
 		   	   if(!o) return;

 		   	   if(!window.captureEvents)
			   {
			   	  o.releaseCapture();
			   }
 		   	   else
			   {
			   	  window.releaseEvents(Event.MOUSEMOVE|Event.MOUSEUP);
			   }

 		   	   var o1 = document.getElementById(Id);
 		   	   if(!o1)
			   {
			      return;
			   }
 		   	   var l0 = o.offsetLeft;
 		   	   var t0 = o.offsetTop;
 		   	   var l = o1.offsetLeft;
 		   	   var t = o1.offsetTop;

 		   	   // alert(l0 + " " + t0 +" "+ l +" "+t);

 		   	   MyMove.Move_e(Id, l0 , t0, l, t,T);
 		   	   document.body.removeChild(o);
 		   	   o = null;
 		  }
 	}


 	this.Move_e = function(Id, l0 , t0, l, t,T)
 	{
 		    if(typeof(window["ct"+ Id]) != "undefined")
			{
				  clearTimeout(window["ct"+ Id]);
			}

 		    var o = document.getElementById(Id);
 		    if(!o) return;
 		    var sl = st = 8;
 		    var s_l = Math.abs(l0 - l);
 		    var s_t = Math.abs(t0 - t);
 		    if(s_l - s_t > 0)
			{
				if(s_t)
				{
					sl = Math.round(s_l / s_t) > 8 ? 8 : Math.round(s_l / s_t) * 6;
				}

 		        else
				{
					sl = 0;
				}
			}
 		    else
			{
				if(s_l)
				{
					st = Math.round(s_t / s_l) > 8 ? 8 : Math.round(s_t / s_l) * 6;
				}
 		        else
			    {
			  	    st = 0;
			    }
			}

 		    if(l0 - l < 0)
			{
				sl *= -1;
			}
 		    if(t0 - t < 0)
			{
				st *= -1;
			}
 		    if(Math.abs(l + sl - l0) < 52 && sl)
			{
 		    	sl = sl > 0 ? 2 : -2;
			}
 		    if(Math.abs(t + st - t0) < 52 && st)
			{
	        	st = st > 0 ? 2 : -2;
			}
 		    if(Math.abs(l + sl - l0) < 16 && sl)
			{
 		    	sl = sl > 0 ? 1 : -1;
			}
 		    if(Math.abs(t + st - t0) < 16 && st)
			{
 		    	st = st > 0 ? 1 : -1;
			}
 		    if(s_l == 0 && s_t == 0)
			{
     		    return;
			}
 		    if(T)
 		    {
 		    	o.style.left = l0 +"px";
 		    	o.style.top = t0 +"px";
 		    	return;
 		    }
 		    else
 		    {
 		    	if(Math.abs(l + sl - l0) < 2)
				{
					o.style.left = l0 +"px";
				}
 		    	else
				{
					o.style.left = l + sl +"px";
				}
 		    	if(Math.abs(t + st - t0) < 2)
				{
					o.style.top = t0 +"px";
				}
 		    	else
				{
					o.style.top = t + st +"px";
				}

 		    	window["ct"+ Id] = window.setTimeout("MyMove.Move_e('"+ Id +"', "+ l0 +" , "+ t0 +", "+ (l + sl) +", "+ (t + st) +","+T+")", 1);
 		    }
 		}
}

function play_wav(wavname,cnt)
{
	return;// 浏览不兼容，暂不处理播放声音
	_play_wav_(wavname);
	if(cnt == undefined) var cnt = 3;
	cnt = cnt -1;
	for(var i=1; i <= cnt; i++)
	{
		setTimeout("_play_wav_('"+wavname+"')",2000*i);
	}
}

function _play_wav_(wavname)
{
	try { 
		var sound = document.getElementById('_qq_wav_');
		if(wavname=="newmsg.wav") sound = document.getElementById('_newmsg_wav_');
		if(wavname=="ring.wav") sound = document.getElementById('_ring_wav_');
		sound.play();
	} catch(err){}
}

if(!isIE()){// firefox innerText define
	HTMLElement.prototype.insertAdjacentHTML=function(where, html){
  var e=this.ownerDocument.createRange();
  e.setStartBefore(this);
  e=e.createContextualFragment(html);
  switch (where)
  {
    case 'beforeBegin': this.parentNode.insertBefore(e, this);break;
    case 'afterBegin': this.insertBefore(e, this.firstChild); break;
    case 'beforeEnd': this.appendChild(e); break;
    case 'afterEnd':
  	if(!this.nextSibling)
  		this.parentNode.appendChild(e);
  	else 
  		this.parentNode.insertBefore(e, this.nextSibling); 
  	break;
  }
	}; 
}
function getEvent() {
	if (typeof(event)=="object") return event;
		func = getEvent.caller;
	while (func != null) {
		var arg0 = func.arguments[0];
	if (arg0) {
		if ((arg0.constructor == Event || arg0.constructor == MouseEvent)
		|| (typeof (arg0) == "object" && arg0.preventDefault && arg0.stopPropagation)) {
			return arg0;
		}
	}
	func = func.caller;
	}
	return null;
}

var tb_pathToImage = "";
function tb_show(title,href,tb_pathToImage)
{
	var params = parseQuery(href);
	var width = parseInt(params["width"])+50;
	var height = parseInt(params["height"])+20;
	var boxparams = {
		opacity: 0.1,
		overlayClose:false,
		open:true,
		iframe:true,
		title:title,
		href:href,
		innerWidth:width,
		innerHeight:height,
		fastIframe:false		
	};
	// 内页弹出
	if(params["inlineId"] != undefined && params["inlineId"] != "")
	{
		boxparams = {
			opacity: 0.1,
			overlayClose:false,
			open:true,
			title:title,
			inline:true,
			href:"#"+params["inlineId"],
			innerWidth:width,
			innerHeight:height
		};
	}
	$.fn.colorbox(boxparams);
}
function tb_remove()
{
	$.fn.colorbox.close();
}

// 表单变化检测
var history_bill_input = "";
function check_form_change(monitor_string)
{
	var current_bill_input = get_input_value(monitor_string);
	if(current_bill_input != history_bill_input) {
		var current_bill_array = parseQuery(current_bill_input);
		var history_bill_array = parseQuery(history_bill_input);
		var monitor_map = parseQuery(monitor_string);
		var change = "";var comma = "";
		$.each(current_bill_array,function(key,value){
			var history_value = history_bill_array[key];
			var keyname = key;
			if(monitor_map[key]!=undefined) keyname = monitor_map[key];
			if(value != history_value) {
				// if(history_value == "") history_value = "为空";
				// if(value == "") value = "为空";
				change += comma + keyname+"[<del>"+history_value+"</del> "+value+"]";
				comma = "\n";
			}
		});
		if(change !="") $("#changelog").val("修改日志:\n"+change);
	}		
}

// 获取当前表单序列化后的字符串
function get_input_value(monitor_string)
{
	var monitor_map = parseQuery(monitor_string);
	var s = "";
	var comma = "";
	$.each(monitor_map,function(key,value){
		var val = $("#"+key).val();
		if($("#"+key).is("select"))
		{
			val = $("#"+key).find("option:selected").text();
		}		
		// 判断字段类型
		s += comma +key+"="+val;
		comma = "&"; 
	});		
	return s;
}