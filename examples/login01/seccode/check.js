if (typeof (checkSeccode) == 'function') {
	if (window.addEventListener) {
		window.addEventListener('load', function() {
			checkSeccode(true);
		}, false);
	} else {
		window.attachEvent('onload', function() {
			checkSeccode(true);
		});
	}
}