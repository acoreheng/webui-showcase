var port = (process.env.VMC_APP_PORT || 3000);
var host = (process.env.VCAP_APP_HOST || 'localhost');
var http = require('http');

http.createServer(function (req, res) {
    var html = '<html>'  
        +'<head>'  
        +'<title>nodejs</title>'  
        +'</head>'  
        +'<body>'  
        +'<h1>NodeJS Demo</h1>'
        +'<p>项目地址：<a href="1.0/index.html">1.0版</a></p>'
        +'<p>搭建教程：<a href="#">大家好</a></p>'
        +'</body>'  
        +'</html>';  
  res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'});
  res.write(html);
  res.end();
}).listen(port, host);